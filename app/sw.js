/* global self, caches */

// use a cacheName for cache versioning
var cacheName = 'v1:static';

//https://github.com/IncredibleWeb/pwa-tutorial/blob/master/demo/sw.js#L4

// during the install phase you usually want to cache static assets
self.addEventListener('install', function (e) {
    // once the SW is installed, go ahead and fetch the resources to make this work offline
    e.waitUntil(
            caches.open(cacheName).then(function (cache, ) {
        var localList = [

            './',
//            './index.html',
            './manifest.json',
            './sw.js',
            './images/vacas-with-joe.png',

            './bower_components/jquery/dist/jquery.js',
            './bower_components/angular/angular.js',
            './bower_components/bootstrap/dist/js/bootstrap.js',
            './bower_components/angular-animate/angular-animate.js',
            './bower_components/angular-resource/angular-resource.js',
            './bower_components/angular-route/angular-route.js',
            './bower_components/angular-sanitize/angular-sanitize.js',
            './bower_components/angular-touch/angular-touch.js',
            './bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            './bower_components/moment/moment.js',

            './scripts/app.js',
            './scripts/controllers/hotels.js',
            './scripts/controllers/day.js',
            './scripts/controllers/activity.js',
            './scripts/controllers/restaurants.js',

            './styles/main.css',
            './bower_components/bootstrap/dist/css/bootstrap.css',

//            './offline.html',

            './views/activity.html',
            './views/home.html',
            './views/day.html',
            './views/nav.html',
            './views/home.html',
            './views/hotels.html',
            './views/hotel.html',
            './views/nav.html',
            './views/restaurants.html',

            './api/hotels/hotels.json',

            './api/food/restaurants.json',

            './api/activities/el-volcan.json',
            './api/activities/potrero.json',

            './api/days/day1.json',
            './api/days/day2.json',
            './api/days/day3.json',
            './api/days/day4.json',
            './api/days/day5.json',
            './api/days/day6.json',
            './api/days/day7.json',
            './api/days/day8.json',
            './api/days/day9.json',
            './api/days/day10.json',
            './api/days/day11.json',
        ];

        return cache.addAll(localList).then(function () {
            self.skipWaiting();
        });
    })
            );
});
//https://github.com/IncredibleWeb/pwa-tutorial/blob/master/demo/sw.js#L4

// when the browser fetches a url
self.addEventListener('fetch', function (event) {
    // either respond with the cached object or go ahead and fetch the actual url
    event.respondWith(
            caches.match(event.request).then(function (response) {
        if (response) {
            console.log('retrieve from cache' + response);
            // retrieve from cache
            return response;
        }
        // fetch as normal
        return fetch(event.request);
    })
            );
});