'use strict';

/**
 * @ngdoc overview
 * @name holidaysApp
 * @description
 * # holidaysApp
 *
 * Main module of the application.
 */
angular
        .module('holidaysApp', [
            'ngAnimate',
           // 'ngCookies',
           // 'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch'
                    // 'FredrikSandell.worker-pool'
        ])
        /**
         * 
         * @param {type} WorkerService
         * @returns {undefined}
         * https://github.com/FredrikSandell/angular-workers/blob/master/example/app.js
         */
//        .run(function (WorkerService) {
//            WorkerService.setAngularUrl('http://localhost:9000/bower_components/angular/angular.js?load=1234');
//            WorkerService.addDependency('pwa', 'holidaysApp', 'scripts/controllers/home-ctrl.js');
//        })
        .config(
        /**
         * 
         * @param {type} $routeProvider
         * @returns {undefined}
         */
        function ($routeProvider) {
            $routeProvider
                    .when('/hotels', {
                        templateUrl: 'views/hotels.html'
                    })
                    .when('/day/:id', {
                        templateUrl: 'views/day.html'
                    })
                    .when('/activity/:id', {
                        templateUrl: 'views/activity.html'
                    })
                    .when('/restaurants/:id', {
                        templateUrl: 'views/restaurants.html'
                    })
                    .when('/hotel/:id', {
                        templateUrl: 'views/hotel.html'
                    })
                    .otherwise({
                        templateUrl: 'views/home.html',
                        redirectTo: '/'
                    });

        })
        .filter('toKm', function () {
            return function (km) {

                if (km !== null) {
                    km = km.toString() + " km";
                }

                return km;

            };

        })
        .filter('toPesos', function (config) {
            return function (activity) {

                var km = activity.km;
                var gasPrice = 0;
                if (km !== null) {
                    gasPrice = km * config.GAS_PRICE;
                }

                var otherCost = activity.cost;

                var cost = otherCost + gasPrice;

                if (cost !== null && cost !== 0) {
                    cost = "$ " + cost.toString();
                }

                if (cost === 0) {
                    cost = null;
                }

                return cost;

            };

        })
        .filter('newlines', ['$sce', function ($sce) {
                return function (text) {
                    return $sce.trustAsHtml(text.replace(/\n/g, '<br/>'));
                };
            }])
        .constant('config', (function () {
            var base = document.baseURI;
            return {
                GAS_PRICE: 3.5,
                GET_BASE: function(base){
                    return base;
                },
                APIS: {
                    GET_DAY: function (dayId) {
                        return base + 'api/days/day' + dayId + '.json';
                    },
                    GET_ACTIVITIES: function (activityId) {
                        return base + 'api/activities/'+ activityId + '.json';
                    },
                    HOTELS: base + 'api/hotels/hotels.json',
                    RESTAURANTS: base + 'api/food/restaurants.json'
                }
            };
        })());

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js').then(function (reg) {
        console.log('Successfully registered service worker', reg);
    }).catch(function (err) {
        console.warn('Error whilst registering service worker', err);
    });
}

window.addEventListener('beforeinstallprompt', function(e) {
  // beforeinstallprompt Event fired

  // e.userChoice will return a Promise.
  // For more details read: https://developers.google.com/web/fundamentals/getting-started/primers/promises
  e.userChoice.then(function(choiceResult) {

    console.log(choiceResult.outcome);

    if(choiceResult.outcome === 'dismissed') {
      console.log('User cancelled home screen install');
    }
    else {
      console.log('User added to home screen');
    }
  });
});

/**
 * @ngdoc overview
 * @name fullGridWebworkersApp
 * @description
 * # fullGridWebworkersApp
 *
 * Main module of the application.
 */
//var app = angular
//        .module('angular-workers-example', ['FredrikSandell.worker-pool'])
//        .run(function (WorkerService) {
//            //WorkerService.setAngularUrl('../bower_components/angular/angular.js');
//            WorkerService.setAngularUrl('https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js');
//            //WorkerService.addDependency(serviceName, moduleName, url);
//        });

//var workerPromise;
//
//app.controller("myChartCtrl", function($scope,WorkerService) {
//
//
//  $scope.awesomeThings = ['HTML5 Boilerplate', 'AngularJS', 'Karma'];
//  $scope.data = {};
//
//  $scope.data.reply1 = 'a';
//  $scope.data.reply2 = 'b';
//  $scope.data.reply3 = 'c';
//
//  $scope.test = function (arg) {
//
//      /**
//      // This contains the worker body.
//      // The function must be self contained.
//      // The function body will be converted to source and passed to the worker.
//      // The input parameter is what will be passed to the worker when it is executed. It must be a serializable object.
//      // The output parameter is a promise and is what the worker will return to the main thread.
//      // All communication from the worker to the main thread is performed by resolving, rejecting or notifying the output promise.
//      // We may optionally depend on other angular services. These services can be used just as in the main thread.
//      // But be aware that no state changes in the angular services in the worker are propagates to the main thread. Workers run in fully isolated contexts.
//      // All communication must be performed through the output parameter.
//   */
//    if (!workerPromise) {
//      workerPromise = WorkerService.createAngularWorker(['input', 'output', '$http', function (input, output, $http) {
//
//        var i=0;
//
//        var callback = function(i) {
//          output.notify(i);
//          i++;
//        };
//
//
//        //for (var i = 0; i < 10; i++) { callback(i); }
//        //var intervalID = setInterval(callback(i), 3000);
//        setInterval(function(){ callback(++i); }, Math.floor((Math.random() * 1000) + 100));
//
//        //output.resolve(true);
//        //output.reject(false);
//
//      }]);
//    }
//
//    workerPromise
//      .then(function success(angularWorker) {
//      //The input must be serializable
//      return angularWorker.run($scope.awesomeThings);
//    }, function error(reason) {
//
//        console.log('callback error');
//        console.log(reason);
//
//        //for some reason the worker failed to initialize
//        //not all browsers support the HTML5 tech that is required, see below.
//      }).then(function success(result) {
//
//        console.log('success');
//        console.log(result);
//
//      //handle result
//    }, function error(reason) {
//        //handle error
//        console.log('error');
//        console.log(reason);
//
//      }, function notify(update) {
//        //handle update
//
//        $scope.data['reply' + arg] += update + '\n';
//        //console.log(arg);
//        //console.log(update);
//      });
//
//  };
//});




//var iconArr = [];
//$('.icon-header').each(function () {
//    var ico = {"src": "images/touch/icon-128x128.png",
//        "sizes": "128x128",
//        "type": "image/png"};
//    $this = $(this);
//    ico.src = $this.attr('href');
//    ico.sizes = $this.attr('sizes');
//    ico.type = "image/png";
//    iconArr.push(ico);
//
//});
//
//
//console.log(iconArr);
//console.log(JSON.stringify(iconArr));


