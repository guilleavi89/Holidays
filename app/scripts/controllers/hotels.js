'use strict';

angular.module('holidaysApp')
        .controller('HotelCtrl', ['$scope', '$http', '$routeParams', 'config',
            function ($scope, $http, $routeParams, config) {

                if ($routeParams && $routeParams.id) {
                    var hotelId = parseInt($routeParams.id);
                }
                var apiUrl = config.APIS.HOTELS;
                $http({
                    method: 'GET',
                    url: config.APIS.HOTELS
                }).then(function successCallback(response) {

                    $scope.hotels = response.data.hotels;

                    for (var i = 0; i < $scope.hotels.length; i++) {
                        if ($scope.hotels[i].id === hotelId) {
                            $scope.hotel = $scope.hotels[i];
                        }
                    }

                }, function errorCallback(response) {
                    
                    console.log('error at ' + apiUrl);
                    console.log(response);
                });
            }]);
                    