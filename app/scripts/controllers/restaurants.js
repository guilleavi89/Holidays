'use strict';

angular.module('holidaysApp')
        .controller('RestaurantsCtrl', ['$scope', '$http', '$routeParams', 'config',
            function ($scope, $http, $routeParams, config) {

                if ($routeParams && $routeParams.id) {
                    var restaurantsId = $routeParams.id;
                }

                var apiUrl = config.APIS.RESTAURANTS;
                $http({
                    method: 'GET',
                    url: config.APIS.RESTAURANTS
                }).then(function successCallback(response) {

                    $scope.restaurantsInCity = response.data[restaurantsId];


                }, function errorCallback(response) {

                    console.log('error at ' + apiUrl);
                    console.log(response);
                });
            }]);
                    