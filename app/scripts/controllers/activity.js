'use strict';

angular.module('holidaysApp')
        .controller('ActivityCtrl', ['$scope', '$http', '$routeParams', 'config',
            function ($scope, $http, $routeParams, config) {
         
                var activityId = "";
                if ($routeParams && $routeParams.id) {
                    activityId = $routeParams.id;
                }
                
                var apiUrl = config.APIS.GET_ACTIVITIES(activityId);

                $http({
                    method: 'GET',
                    url: config.APIS.GET_ACTIVITIES(activityId)
                }).then(function successCallback(response) {
                    $scope.activity = response.data.activity;
                    $scope.city = response.data.city;  
                    $scope.info = response.data.info;
                    $scope.text = response.data.text;
                    $scope.pictures = response.data.pictures;

                }, function errorCallback(response) {
                    console.log('error at ' + apiUrl);
                    console.log(response);
                });
            }]);
                    