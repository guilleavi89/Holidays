'use strict';

angular.module('holidaysApp')
        .controller('DayCtrl', ['$scope', '$http', '$routeParams', 'config',
            function ($scope, $http, $routeParams,config) {

                var dayId = 0;
                if ($routeParams && $routeParams.id) {
                    dayId = parseInt($routeParams.id);
                }

                var apiUrl = config.APIS.GET_DAY(dayId);

                $http({
                    method: 'GET',
                    url: config.APIS.GET_DAY(dayId)
                }).then(function successCallback(response) {
                    $scope.activities = response.data.activities;
                    $scope.city = response.data.city;
                    $scope.day = response.data.day;
                    $scope.daystarts = response.data.daystarts;

                    $scope.getTotalPesos = function () {
                        var total = 0;
                        var totalkm = 0;

                        var gasprice = config.GAS_PRICE;

                        for (var i = 0; i < $scope.activities.length; i++) {
                            var activity = $scope.activities[i];
                            total += (activity.cost);
                            totalkm += (activity.km);
                        }

                        total = total + (totalkm * gasprice);

                        return total;
                    };

                    $scope.getTotalKm = function () {

                        var totalkm = 0;

                        for (var i = 0; i < $scope.activities.length; i++) {
                            var activity = $scope.activities[i];
                            totalkm += (activity.km);
                        }

                        return totalkm;
                    };

                    $scope.getStartTime = function (activity, activities) {

                        if (activity.id === 1) {
                            return $scope.daystarts;
                        } else {

                            var dayStart = moment($scope.daystarts, 'HH:mm');
                            for (var i = 1; i < activity.id; i++) {

                                dayStart = moment(dayStart, 'HH:mm');

                                dayStart = dayStart.add(activities[i - 1].duration, 'hours').format('HH:mm');
                            }

                            return dayStart;
                        }
                    };

                    $scope.getEndTime = function (activity, activities) {

                        var dayEnd = moment($scope.daystarts, 'HH:mm');

                        for (var i = 1; i <= activity.id; i++) {

                            dayEnd = moment(dayEnd, 'HH:mm');

                            dayEnd = dayEnd.add(activities[i - 1].duration, 'hours').format('HH:mm');
                        }

                        return dayEnd;
                    };

                }, function errorCallback(response) {
                    console.log('error at ' + apiUrl);
                    console.log(response);
                });
            }]);
                    